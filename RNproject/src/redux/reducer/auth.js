const initialState = {
    username: null,
    isLoggedIn: false,
};

const auth = (state = initialState , action) => {
    switch (action.type) {
        case 'LOGIN' : {
            return {
                    username: action.username,
                    isLoggedIn: true,
                };
        }
        default :
            return state;
    }
};

export default auth;
