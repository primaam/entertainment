import React from 'react';
import {FlatList} from 'react-native';
import PromotionItem from './PromotionItemComponent';
import axios from 'axios';

export default class PromotionList extends React.Component{
    constructor (props){
        super(props);
        this.state = {
            listPromotions: [],
        }
    }

    componentDidMount(){
        this.getPromotions()
    }
    //     axios ({
    //         method: 'GET',
    //         url: 'http://localhost:5000/promotions',
    //     })
    //     .then ((res) =>{
    //         this.setState ({
    //             listPromotions: res.data,
    //         });
    //     })
    //     .catch((err) =>{
    //         console.error(err); 
    //     });
    async getPromotions() {
        try {
            const resAwait = await axios ({
                method: 'GET',
                url: 'http://localhost:5000/promotions',
            });
            this.setState ({
                listPromotions: resAwait.data,
            }); 
        } catch (err) {
            console.error(err);
        }
    }

    render(){
        return(
            <FlatList
                pagingEnabled={true}
                Horizontal={true}
                showsHorizontalScrollIndicator={false}
                data= {this.state.listPromotions}
                renderItem= {({item}) => (
                    <PromotionItem url={item.url} Image={item.preview_image}/>
                )}
                keyExtractor = {(item)=> String(item.id)}
                />
        );
    }
}


