import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class JokesItem extends React.Component{
    constructor(props){
        super(props);
    }

componentDidMount(){
    this.getJokes()
  }

async getJokes(){
  try {
      const res = await axios ({
        method: 'GET',
        url: 'http://localhost:5000/jokes',
      });

      this.setState ({listJokes: res.data});
      this.timerJokes = setInterval (() => {
        const randomNumber = Math.floor ( Math.random() * this.state.listJokes.length);
            this.setState ({selectedJokes: this.state.listJokes[randomNumber]});
          }, 5000);
      } 
      catch(err){
        console.error(err);  
      }
    }

    // render ya
    render (){
        return (
            <View style={styles.itemContainer}>
                <Text style={styles.itemPertanyaan}>{this.props.pertanyaan}</Text>
                <Text style={styles.itemJawaban}>{this.props.jawaban}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
      backgroundColor: 'white',
      borderRadius: 15,
      margin: 10,
      padding: 20,
      alignItems: 'stretch',
      height: 130,
      elevation: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 3},
      shadowOpacity: 0.5,
      shadowRadius: 15,
    },
    itemPertanyaan: {
      fontFamily: 'sego-ui',
      fontSize: 18,
      fontWeight: 'bold',
    },
    itemJawaban: {
      fontFamily: 'sego-ui',
      fontSize: 18,
      fontStyle: 'italic',
    },
  });
    