import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainNavigator from './MainNavigator';
import LoginScreen from '../screen/LoginScreen';
import {NavigationContainer} from '@react-navigation/native';
import {connect} from 'react-redux';

const Stack = createStackNavigator();

function AppStack (props){
    return (
        <NavigationContainer>
            <Stack.Navigator>
                {props.statusLogin ? (
                    <Stack.Screen 
                    options= {{headerShown: false}}
                    name='Main' 
                    component={MainNavigator}/>
                
                ):(
                    <Stack.Screen name='Login' component={LoginScreen}/>
                )}
            </Stack.Navigator>
        </NavigationContainer>
    );
}

const mapStateToProps = (state) => ({
    statusLogin: state.auth.isLoggedIn,
});
  
const mapDispatchToProps = (dispatch) => ({});
  
export default connect(mapStateToProps, mapDispatchToProps)(AppStack);