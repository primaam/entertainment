import {createStore} from 'redux';
import rootReducer from './redux/reducer/index';
import createSagaMiddleware from 'redux-saga';
import authSaga from '../src/saga/auth';

const sagaMiddleware = createSagaMiddleware()
const store = createStore((rootReducer), applyMiddleware(sagaMiddleware));

sagaMiddleware.run(authSaga)
export default store;
