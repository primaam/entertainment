import React, { useState } from'react';
import {Button, View, TextInput, Text} from 'react-native';
import {connect} from 'react-redux';

function LoginScreen(props){
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null); 

    return (
        <View>
            <Text>kuy login</Text>
            <TextInput
                onChangeText= {(text) => setUsername(text)}
                value= {username}
                style= {{backgroundColor: '#ffffff'}}
                placeholder= 'username'
            />
            <TextInput
                onChangeText= {(text) => setPassword(text)}
                value= {password}
                style= {{backgroundColor: '#ffffff'}}
                secureTextEntry={true}
                placeholder= 'password'
            />
            <Button
            color= '#4444' 
            onPress={()=> props.processLogin(username)} 
            title='Masuk'>
            </Button>
        </View>
    );    
}

const mapStateToProps = (state) => ({});
  
const mapDispatchToProps = (dispatch) => ({
      processLogin: (textUsername) => dispatch({type: 'LOGIN', username:textUsername}),
  });
  
  export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
  