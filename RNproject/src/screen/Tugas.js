import React, {useState , useEffect} from 'react';
import { Text, View, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native';


export default function Tugas() {
    const [name, setName] = useState("");
    const [product, setProduct] = useState("");
    const [review, setReview] = useState("");

    function write() {
        if (!name) {
            alert ("harap lengkapi nama");
        } else if (!product){
            alert ("harap lengkapi produk");
        }else if (!review){
            alert ("harap lengkapi review");
        }else {
            alert ("oke banget");
        }
    }

    return (
        <View style={{backgroundColor: 'black', padding: 20}}>
            <View style={{alignItems: 'center', backgroundColor: 'pink'}}> 
                <Image 
                source={require('../../img/icon-completed-task.png')}
                style={styles.image}
                />
            </View>
            <View>
                <TextInput
                onChangeText = {(text) => setName(text)}
                value={name}
                style={styles.input}
                placeholder="nama"
                />
                <TextInput
                onChangeText = {(text) => setProduct(text)}
                value={product}
                style={styles.input}
               placeholder="produk yang digunakan"
                />
                <TextInput
                onChangeText = {(text) => setReview(text)}
                value={review}
                style={styles.input}
                placeholder="Tulis pendapatmu disini"
                />
            </View>
            <View>
                <TouchableOpacity
                onPress={()=> write()}
                style={styles.button}
                >
                <Text style={styles.textButton}>KIRIM</Text>
                </TouchableOpacity>
            </View>
        </View>
        )
}

const styles = StyleSheet.create({
    image: {
        margin: 20,
        width: 100,
        height: 100,
    },
    input:{
        marginHorizontal: 20,
        marginVertical: 5,
        padding: 10,
        alignItems: 'flex-start',
        backgroundColor: '#999', 
        color: '#333',
        borderRadius: 25
    },
    button: {
        marginHorizontal: 30,
        alignItems: 'center',
        color: '#666',
        borderRadius: 100
    },
    textButton: {
        textAlign: 'center'
    }
})




